#!/bin/bash -l
#SBATCH -J gitlab_runner
#SBATCH -p bluefield1
#SBATCH -A do009
#SBATCH -t 01:00:00
#SBATCH -w b116

# load any modules you might need for the pipeline execution...
module load oneAPI intel_mpi gnu_comp/11.1.0

gitlab_runner run --config $HOME/.gitlab-runner/config.toml